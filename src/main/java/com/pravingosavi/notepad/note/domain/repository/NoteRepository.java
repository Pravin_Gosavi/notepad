package com.pravingosavi.notepad.note.domain.repository;


import com.pravingosavi.notepad.note.domain.model.Note;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface NoteRepository extends MongoRepository<Note, String> {

}
