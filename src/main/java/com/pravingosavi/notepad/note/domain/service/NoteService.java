package com.pravingosavi.notepad.note.domain.service;

import com.pravingosavi.notepad.note.domain.model.Note;

import java.util.List;

public interface NoteService {

	Note create(Note note);
	void delete(Note note);
	List<Note> findAll();
}
