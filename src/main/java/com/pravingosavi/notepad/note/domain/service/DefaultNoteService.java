package com.pravingosavi.notepad.note.domain.service;

import com.pravingosavi.notepad.note.domain.model.Note;
import com.pravingosavi.notepad.note.domain.repository.NoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DefaultNoteService implements NoteService {
	
	@Autowired
	private NoteRepository noteRepository;

	@Override
	public Note create(Note note) {
		return noteRepository.save(note);
	}

	@Override
	public void delete(Note note) {
		noteRepository.delete(note);
	}

	@Override
	public List<Note> findAll() {
		return noteRepository.findAll();
	}
}
