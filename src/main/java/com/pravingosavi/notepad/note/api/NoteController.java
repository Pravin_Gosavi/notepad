package com.pravingosavi.notepad.note.api;

import com.pravingosavi.notepad.note.domain.model.Note;
import com.pravingosavi.notepad.note.domain.service.NoteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.List;

@Controller
public class NoteController {

	@Autowired
	private NoteService noteService;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@RequestMapping("/")
	public ModelAndView index() {
		ModelAndView modelAndView = new ModelAndView("index");
		List<Note> notes = noteService.findAll();
		modelAndView.addObject("notes", notes);
		return modelAndView;
	}

	@PostMapping("/notes")
	@ResponseBody
	@ResponseStatus(code = HttpStatus.CREATED)
	public Note create(@RequestBody @Valid Note note) {
		logger.debug("Create note request: {}", note);
		return noteService.create(note);
	}
}
