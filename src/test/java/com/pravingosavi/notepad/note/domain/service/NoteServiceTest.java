package com.pravingosavi.notepad.note.domain.service;

import com.pravingosavi.notepad.note.domain.model.Note;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class NoteServiceTest {
	
	@Autowired
	private NoteService noteService;
	
	private Note note;
	
	@Before
	public void setUp() {
		note = new Note("Kubernetes", "Best container orchestration tool ever");
	}
	
	@After
	public void destroy() {
		noteService.delete(note);
	}
	
	@Test
	public void shouldCreateNoteWithTitleAndContent() {
		Note createdNote = noteService.create(note);
		assertThat(createdNote.getId(), notNullValue());
		assertThat(createdNote.getWordCount(), is(5));
	}
}
