FROM openjdk:8-alpine
ADD target/notepad-1.0.0-SNAPSHOT.jar notepad-1.0.0-SNAPSHOT.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","notepad-1.0.0-SNAPSHOT.jar"]
